package dev.tocode;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class MicroInjector {
    private static final ConcurrentHashMap<Class<?>, Object> singletonRepository = new ConcurrentHashMap<>();
    private static final Map<Class<?>, Class<?>> implementations = new HashMap<>();
    private static final Map<Class<?>, Class<?>> abstracts = new HashMap<>();
    private static final Map<Class<?>, Constructor<?>> constructors = new HashMap<>();
    private static final Map<Class<?>, List<Child>> children = new HashMap<>();

    private MicroInjector() {}

    public static Object run(Class<?> rootClass) {
        var userDefinedTypes = new ArrayList<Class<?>>();
        try {
            // Finds all users Java files in this project and populates a list of user-defined types.
            var rootPackagePathStr = MicroInjector.class.getPackageName().replace('.', '/');
            var matcherPattern = "glob:**" + rootPackagePathStr + "**?.java";
            var javaFileMatcher = FileSystems.getDefault().getPathMatcher(matcherPattern);
            Files.walkFileTree(Path.of(""), new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                    if (javaFileMatcher.matches(file)) {
                        var pathStr = file.toString();
                        var className = pathStr.substring(pathStr.indexOf(rootPackagePathStr), pathStr.lastIndexOf('.'))
                                               .replace('/', '.');
                        try {
                            userDefinedTypes.add(Class.forName(className));
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        var userConcreteClasses = new ArrayList<Class<?>>();
        var ambiguousImplementations = new HashMap<Class<?>, Class<?>>();
        // Mapping abstract and concrete classes.
        userDefinedTypes.stream()
                        .filter(aClass -> !aClass.isEnum() &&
                                          !aClass.isRecord() &&
                                          !aClass.isAnnotation() &&
                                          !Modifier.isAbstract(aClass.getModifiers()))
                        .forEach(concreteClass -> {
                            userConcreteClasses.add(concreteClass);
                            // Mapping to itself for non-abstract class
                            implementations.put(concreteClass, concreteClass);
                            var isPrimary = concreteClass.isAnnotationPresent(Primary.class);
                            userDefinedTypes.stream()
                                            .filter(aClass -> !aClass.equals(concreteClass) &&
                                                              aClass.isAssignableFrom(concreteClass))
                                            .forEach(abstractClass -> {
                                                // Mapping for interfaces and abstract classes
                                                if (!implementations.containsKey(abstractClass) || isPrimary) {
                                                    implementations.put(abstractClass, concreteClass);
                                                } else {
                                                    ambiguousImplementations.put(concreteClass, abstractClass);
                                                }
                                                if (abstracts.containsKey(concreteClass)) {
                                                    // If an abstract class inherits from another abstract class,
                                                    // in this case will map the abstract class to its super class.
                                                    var otherAbstractClass = abstracts.get(concreteClass);
                                                    if (otherAbstractClass.isAssignableFrom(abstractClass)) {
                                                        abstracts.put(abstractClass, otherAbstractClass);
                                                    }
                                                } else {
                                                    abstracts.put(concreteClass, abstractClass);
                                                }
                                            });
                        });
        // If there are multiple implementations of an abstract class, then
        // one of the implementations should be annotated with @Primary annotation.
        ambiguousImplementations.forEach((impl, abstr) -> {
            var savedImpl = implementations.get(abstr);
            if (!savedImpl.isAnnotationPresent(Primary.class)) {
                var errorMessage = new StringBuilder("\n Ambiguity ! ");
                errorMessage.append(abstr.getSimpleName()).append(" has multiple implementations (");
                ambiguousImplementations.forEach((i, a) -> {
                    if (a.equals(abstr)) errorMessage.append(i.getSimpleName()).append(", ");
                });
                errorMessage.append(savedImpl.getSimpleName());
                errorMessage.append(")\n but no implementation with @Primary annotation was found among them.\n");
                throw new AmbiguousImplementationsException(errorMessage.toString());
            }
        });

        // Mapping relation parent->children for each class
        userConcreteClasses.forEach(concreteClass -> {
            // Mapping constructor for each concrete class
            var isSingleton = false;
            var numberOfAnnotatedConstructors = 0;
            Constructor<?> defaultConstructor = null;

            // Choose the right constructor
            for (var constructor : concreteClass.getDeclaredConstructors()) {
                // Prefer annotated constructor if present
                if (hasAnnotation(constructor)) {
                    defaultConstructor = constructor;
                    isSingleton = constructor.isAnnotationPresent(Singleton.class);
                    numberOfAnnotatedConstructors++;
                } else if (constructor.getParameterCount() == 0) {
                    defaultConstructor = constructor;
                }
                if (numberOfAnnotatedConstructors > 1) {
                    throw new MultipleAnnotatedConstructorsException(concreteClass);
                }
            }
            if (defaultConstructor == null) {
                throw new AppropriateConstructorNotFoundException(concreteClass);
            }

            constructors.put(concreteClass, defaultConstructor);
            for (var parameter : defaultConstructor.getParameters()) {
                var parameterType = parameter.getType();
                var parameterTypeImpl = implementations.get(parameterType);
                if (parameterTypeImpl == null) {
                    throw new IllegalArgumentException(
                            "\n! Appropriate constructor not found for auto-wired parameter of type " +
                            parameterType.getSimpleName() +
                            " in constructor of " + concreteClass.getSimpleName());
                }
                // Prefers a user-specified scope from parameter if present
                if (hasAnnotation(parameter)) isSingleton = parameter.isAnnotationPresent(Singleton.class);

                if (!children.containsKey(concreteClass)) children.put(concreteClass, new ArrayList<>());
                children.get(concreteClass).add(new Child(parameterType, isSingleton, false));
            }
        });

        return createInstance(rootClass, true, false);
    }

    private static void checkForCircularDependency(Class<?> parent, List<Class<?>> lastChecked) {
        var parentImpl = implementations.get(parent);
        if (lastChecked.contains(parentImpl)) {
            var iterator = lastChecked.iterator();
            while (iterator.hasNext()) {
                var c = iterator.next();
                if (c.equals(parentImpl)) break;
                iterator.remove();
            }
            throw new CircularDependencyException(lastChecked);
        }
        lastChecked.add(parentImpl);

        var childrenList = children.get(parentImpl);
        if (childrenList != null) {
            childrenList.forEach(child -> checkForCircularDependency(child.castClass(), lastChecked));
        }

        lastChecked.remove(parentImpl);
    }

    private static <T> T createInstance(Class<T> castClass, boolean isSingleton, boolean isProxy) {
        var classImpl = implementations.get(castClass);

        if (!isProxy && isSingleton && singletonRepository.containsKey(classImpl)) {
            return castClass.cast(singletonRepository.get(classImpl));
        }

        Object[] args = null;
        var parameters = children.get(classImpl);
        if (parameters != null) {
            var parametersSize = parameters.size();
            args = new Object[parametersSize];
            for (var argIndex = 0; argIndex < parametersSize; argIndex++) {
                var parameter = parameters.get(argIndex);
                args[argIndex] = createInstance(parameter.castClass(), parameter.isSingleton(), false);
            }
        }

        T instance;
        try {
            var constructor = constructors.get(classImpl);
            constructor.setAccessible(true);
            if (args == null) instance = castClass.cast(constructor.newInstance());
            else instance = castClass.cast(constructor.newInstance(args));
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

        Arrays.stream(classImpl.getDeclaredFields())
              .filter(MicroInjector::hasAnnotation)
              .forEach(annotatedField -> {
                  var fieldType = annotatedField.getType();
                  var fieldImpl = implementations.get(fieldType);

                  if (!fieldType.isInterface()) {
                      throw new IllegalArgumentException("\nLazyWired field must be an interface ! ");
                  }
                  checkForCircularDependency(fieldImpl, new ArrayList<>());

                  var fieldName = annotatedField.getName();
                  var isSingletonField = annotatedField.isAnnotationPresent(Singleton.class);
                  Object fieldInstance;
                  if (!isSingletonField || !singletonRepository.containsKey(fieldImpl)) {
                      fieldInstance = fieldType.cast(Proxy.newProxyInstance(fieldType.getClassLoader(),
                                                                            new Class<?>[]{fieldType},
                                                                            new LazyInjector<>(fieldType,
                                                                                               fieldName,
                                                                                               isSingleton,
                                                                                               instance)));
                      if (isSingletonField) singletonRepository.put(fieldImpl, fieldInstance);
                  } else {
                      fieldInstance = singletonRepository.get(fieldImpl);
                  }
                  setField(instance, annotatedField, fieldType, fieldInstance);
              });

        if (isSingleton && isProxy) singletonRepository.replace(classImpl, instance);
        else if (isSingleton) singletonRepository.put(classImpl, instance);
        return instance;
    }

    private static <C, F> void setField(C parentInstance, Field field, Class<F> fieldType, Object fieldInstance) {
        try {
            field.setAccessible(true);
            field.set(parentInstance, fieldType.cast(fieldInstance));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private static <T extends AnnotatedElement> boolean hasAnnotation(T o) {
        return o.isAnnotationPresent(Singleton.class) || o.isAnnotationPresent(Prototype.class);
    }

    @Target({ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Primary {
    }

    @Target({ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Prototype {
    }

    @Target({ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Singleton {
    }

    private static class CircularDependencyException extends RuntimeException {
        CircularDependencyException(List<Class<?>> classesInCircle) {
            super(makeErrorMessage(classesInCircle));
        }

        private static String makeErrorMessage(List<Class<?>> classesInCircle) {
            var listSize = classesInCircle.size();

            var firstClass = classesInCircle.get(0);
            var lastClass = classesInCircle.get(listSize - 1);
            var firstClassName = getClassName(firstClass);
            var lastClassName = getClassName(lastClass);

            var errorMessage = new StringBuilder("\n! Circular dependency between constructors of ");
            errorMessage.append(firstClassName).append(" and ").append(lastClassName).append("\n");

            for (var i = 0; i < listSize; i++) {
                var className = getClassName(classesInCircle.get(i));
                errorMessage.append(className).append(" --> ");
            }
            errorMessage.append("...\n");

            return errorMessage.toString();
        }

        private static String getClassName(Class<?> c) {
            var classImpl = implementations.get(c);
            var classAbst = abstracts.get(c);
            if (classAbst != null && !classAbst.equals(classImpl)) {
                return classImpl.getSimpleName() + "(" + classAbst.getSimpleName() + ")";
            } else return classImpl.getSimpleName();
        }
    }

    private static class MultipleAnnotatedConstructorsException extends RuntimeException {
        public MultipleAnnotatedConstructorsException(Class<?> concreteClass) {
            super("\n! More than one annotated constructor was found for " + concreteClass.getSimpleName());
        }
    }

    private static class AppropriateConstructorNotFoundException extends RuntimeException {
        public AppropriateConstructorNotFoundException(Class<?> concreteClass) {
            super("\n! No annotated or default constructor was found for " + concreteClass.getSimpleName());
        }
    }

    private static class AmbiguousImplementationsException extends RuntimeException {
        public AmbiguousImplementationsException(String errorMessage) {
            super(errorMessage);
        }
    }

    private static class LazyInjector<F, P> implements InvocationHandler {
        private final Class<F> castClass;
        private final boolean isSingleton;
        private final P parentInstance;
        private final String fieldName;
        private F fieldInstance;

        LazyInjector(Class<F> castClass, String fieldName, boolean isSingleton, P parentInstance) {
            this.castClass = castClass;
            this.fieldName = fieldName;
            this.isSingleton = isSingleton;
            this.parentInstance = parentInstance;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) {
            if (fieldInstance == null) fieldInstance = createInstance(castClass, isSingleton, true);
            try {
                var field = parentInstance.getClass().getDeclaredField(fieldName);
                setField(parentInstance, field, castClass, fieldInstance);
                return method.invoke(fieldInstance, args);
            } catch (NoSuchFieldException | IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private record Child(
            Class<?> castClass,
            boolean isSingleton,
            boolean isLazy
    ) {}
}
